package com.revolut.transfer.dto.enums;

public enum TransferStatus {
    SUCCESS,
    ERROR
}
