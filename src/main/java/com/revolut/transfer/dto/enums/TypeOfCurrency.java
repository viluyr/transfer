package com.revolut.transfer.dto.enums;

public enum TypeOfCurrency {
    DOLLAR,
    EURO
}
