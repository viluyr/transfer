package com.revolut.transfer.dto;

import com.revolut.transfer.dto.enums.TypeOfCurrency;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode
public class AccountEntity {
    private long accountID;
    private double amount;
    private TypeOfCurrency currency;
    private String active;
    private long transferId;

}
