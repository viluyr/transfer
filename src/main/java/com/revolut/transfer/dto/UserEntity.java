package com.revolut.transfer.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode
public class UserEntity {
    private long id;
    private String name;
    private String surname;
    private long accountID;
}
