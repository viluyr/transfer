package com.revolut.transfer.dto;

import com.revolut.transfer.dto.enums.TransferStatus;
import com.revolut.transfer.dto.enums.TypeOfCurrency;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode
@Builder
public class TransferEntity {
    private long id;
    private long fromAccountID;
    private long toAccountID;
    private double amountOfTransferMoney;
    private TypeOfCurrency currency;
    private TransferStatus status;
    private Date dateOfTransactions;
}
