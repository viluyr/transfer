package com.revolut.transfer.exception;

public class UnprocessableEntityException extends Throwable {
    public UnprocessableEntityException(final String message) {
        super(message);
    }
}
