package com.revolut.transfer.exception;

public class BalanceException extends Throwable {
    public BalanceException(String format) {
        super(format);
    }
}
