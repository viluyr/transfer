package com.revolut.transfer.exception;

public class UnProcessableEntityException extends Throwable {
    public UnProcessableEntityException(final String message) {
        super(message);
    }
}
