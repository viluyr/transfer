package com.revolut.transfer.exception;

public class ServerStartException extends Throwable {
    public ServerStartException(String format) {
        super(format);
    }
}
