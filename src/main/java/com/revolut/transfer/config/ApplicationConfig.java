package com.revolut.transfer.config;

import org.glassfish.jersey.server.ResourceConfig;

public class ApplicationConfig extends ResourceConfig {

    public ApplicationConfig() {
        packages(true, "com.revolut.transfer");
        register(new ApplicationBinder());
    }
}
