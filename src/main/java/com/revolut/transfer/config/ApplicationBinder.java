package com.revolut.transfer.config;

import com.revolut.transfer.init.MyBatisInitializer;
import com.revolut.transfer.service.AccountService;
import com.revolut.transfer.service.UserService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

public class ApplicationBinder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(UserService.class).to(UserService.class);
        bind(AccountService.class).to(AccountService.class);
        bind(MyBatisInitializer.class).to(MyBatisInitializer.class);
    }
}
