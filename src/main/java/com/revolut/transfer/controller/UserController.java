package com.revolut.transfer.controller;

import com.revolut.transfer.dto.UserEntity;
import com.revolut.transfer.exception.UnProcessableEntityException;
import com.revolut.transfer.service.UserService;
import org.apache.ibatis.javassist.NotFoundException;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserController {

    private  UserService userService;

    @Inject
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @POST
    @Path("/create")
    public Response addUser(UserEntity userEntity) throws UnProcessableEntityException {
        if (userEntity != null) {
            userService.createUser(userEntity);
        }
        return Response.ok(userEntity).build();
    }

    @GET
    @Path("/{id}")
    public UserEntity getUsers(@PathParam("id") long id) {
        UserEntity user = userService.getUserByID(id);
        return user;
    }

    @PUT
    @Path("/update")
    public Response updateUser(@PathParam("id") long id, @PathParam("accountId") long accountId) {
        userService.setAccountIDByUserId(id, accountId);
        return Response.ok().build();
    }

    @DELETE
    @Path("/delete/{id}")
    public Response deleteUser(@PathParam("id") Long id) throws NotFoundException {
        userService.delete(id);
        return Response.ok().build();
    }
}
