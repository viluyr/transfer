package com.revolut.transfer.controller;

import com.revolut.transfer.dto.AccountEntity;
import com.revolut.transfer.dto.TransferEntity;
import com.revolut.transfer.exception.BalanceException;
import com.revolut.transfer.exception.UnProcessableEntityException;
import com.revolut.transfer.service.AccountService;
import org.apache.ibatis.javassist.NotFoundException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/bank")
public class AccountController {
    private final AccountService accountService;

    @Inject
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @POST
    @Path("/create/{accountID}")
    @Produces("application/json")
    @Consumes("application/json")
    public long createAccountWithId(AccountEntity accountEntity) throws UnProcessableEntityException {
        return accountService.createAccount(accountEntity);
    }

    @DELETE
    @Path("/delete/{id}")
    @Consumes("application/json")
    public Response deleteAccount(@PathParam("id") long id) throws NotFoundException {
        accountService.delete(id);
        return Response.ok().build();
    }

    @POST
    @Path("/transfer")
    @Consumes("application/json")
    public Response createTransfer(TransferEntity transferEntity) throws BalanceException {
        accountService.makeTransfer(transferEntity.getFromAccountID(), transferEntity.getToAccountID(),
                transferEntity.getAmountOfTransferMoney(), transferEntity.getCurrency());
        return Response.ok().build();
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getAccount(@PathParam("id") long id) throws UnProcessableEntityException {
        AccountEntity accountEntity = accountService.getAccountByID(id);
        return Response.ok(accountEntity).build();
    }


}
