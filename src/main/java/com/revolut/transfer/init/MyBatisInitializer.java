package com.revolut.transfer.init;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;
import java.io.Reader;
import java.sql.Connection;


@Slf4j
public class MyBatisInitializer {
    private static SqlSessionFactory factory;
    private static String RESOURCE = "my_context.xml";
    private static String INIT_DATABASE = "h2db.sql";
@SneakyThrows
   private  void initDatabaseFactory() {
        try (
            InputStream reader = Resources.getResourceAsStream(RESOURCE);
            Reader secondReader= Resources.getResourceAsReader(INIT_DATABASE)){
            log.debug("Work with resources{}", reader);
            factory = new SqlSessionFactoryBuilder().build(reader);
            SqlSession session = factory.openSession();
            Connection conn = session.getConnection();
            ScriptRunner runner = new ScriptRunner(conn);
            runner.setLogWriter(null);
            runner.runScript(secondReader);
        }
    }

    public  SqlSessionFactory getSqlSessionFactory()  {
        if (factory == null) {
            initDatabaseFactory();
        }
        return factory;
    }

}
