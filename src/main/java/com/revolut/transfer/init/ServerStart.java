package com.revolut.transfer.init;

import com.revolut.transfer.config.ApplicationConfig;
import com.revolut.transfer.exception.ServerStartException;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

@Slf4j
public class ServerStart {

    public void initServer() throws ServerStartException {
        Server server = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        ServletHolder servletHolder = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        servletHolder.setInitParameter("javax.ws.rs.Application", ApplicationConfig.class.getCanonicalName());
        try {
            server.start();
            server.join();
        } catch (Exception e) {
            throw new ServerStartException("Server can't start");
        } finally {
            server.destroy();
        }
    }
}
