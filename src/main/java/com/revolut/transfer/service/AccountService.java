package com.revolut.transfer.service;

import com.revolut.transfer.dao.AccountMapper;
import com.revolut.transfer.dao.TransferMapper;
import com.revolut.transfer.dto.AccountEntity;
import com.revolut.transfer.dto.TransferEntity;
import com.revolut.transfer.dto.enums.TransferStatus;
import com.revolut.transfer.dto.enums.TypeOfCurrency;
import com.revolut.transfer.exception.BalanceException;
import com.revolut.transfer.exception.UnProcessableEntityException;
import com.revolut.transfer.init.MyBatisInitializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.javassist.NotFoundException;
import org.apache.ibatis.session.SqlSession;

import javax.inject.Inject;
import java.util.Date;

@Slf4j
public class AccountService {

    private final MyBatisInitializer myBatisInitializer;

    @Inject
    public AccountService(MyBatisInitializer myBatisInitializer) {
        this.myBatisInitializer = myBatisInitializer;
    }

    private Boolean withdraw(long fromAccountId, long toAccountId, double requestedAmount) throws BalanceException {
        try (SqlSession sqlSession = myBatisInitializer.getSqlSessionFactory().openSession()) {
            log.debug("Make withdrawal {}", requestedAmount);
            AccountMapper accountMapper = sqlSession.getMapper(AccountMapper.class);
            AccountEntity fromAcc = accountMapper.getAccountByID(fromAccountId);
            AccountEntity toAcc = accountMapper.getAccountByID(toAccountId);
            double whatAccountReallyHave = fromAcc.getAmount();

            if (requestedAmount > whatAccountReallyHave) {
                throw new BalanceException(String.format("Current account balance %1$,.2f ", whatAccountReallyHave));
            } else {
                fromAcc.setAmount(whatAccountReallyHave - requestedAmount);
                toAcc.setAmount(toAcc.getAmount() + requestedAmount);
                sqlSession.commit();
                return true;
            }
        }
    }

    public long createAccount(final AccountEntity accountEntity) throws UnProcessableEntityException {
        try (SqlSession sqlSession = myBatisInitializer.getSqlSessionFactory().openSession()) {
            AccountMapper accountMapper = sqlSession.getMapper(AccountMapper.class);
            if (accountEntity == null) {
                throw new UnProcessableEntityException("Failed to create Account");
            }
            log.debug("Create account{}", accountEntity);
            accountMapper.createAccount(accountEntity);
            return accountEntity.getAccountID();
        }
    }

    public AccountEntity getAccountByID(final long id) throws UnProcessableEntityException {
        try (SqlSession sqlSession = myBatisInitializer.getSqlSessionFactory().openSession()) {
            AccountMapper accountMapper = sqlSession.getMapper(AccountMapper.class);
            if (accountMapper.getAccountByID(id) == null) {
                throw new UnProcessableEntityException("We don't have this account");
            }
            return accountMapper.getAccountByID(id);
        }
    }

    public AccountEntity updateAccountBalanceById(final long id, double amount) {
        try (SqlSession sqlSession = myBatisInitializer.getSqlSessionFactory().openSession()) {
            AccountMapper accountMapper = sqlSession.getMapper(AccountMapper.class);
            return accountMapper.updateAccountBalanceById(id, amount);
        }
    }

    public Boolean delete(Long id) throws NotFoundException {
        try (SqlSession sqlSession = myBatisInitializer.getSqlSessionFactory().openSession()) {
            AccountMapper accountMapper = sqlSession.getMapper(AccountMapper.class);
            if (!accountMapper.delete(id))
                throw new NotFoundException("Invalid Account id");
            log.debug("Delete account{}", id);
            return true;
        }
    }

    public TransferEntity makeTransfer(long fromAccountId, long toAccountId, double amountForTransfer,
                                       TypeOfCurrency typeOfCurrency)
            throws BalanceException {
        try (SqlSession sqlSession = myBatisInitializer.getSqlSessionFactory().openSession()) {
            log.debug("Start transfer process{}",amountForTransfer, typeOfCurrency);
            TransferMapper transferMapper = sqlSession.getMapper(TransferMapper.class);

            Date date = new Date();
            TransferEntity createNewTransfer = TransferEntity.builder()
                    .amountOfTransferMoney(amountForTransfer)
                    .fromAccountID(fromAccountId)
                    .toAccountID(toAccountId)
                    .currency(typeOfCurrency)
                    .dateOfTransactions(date).build();

            if (withdraw(fromAccountId, toAccountId, amountForTransfer)) {
                createNewTransfer.setStatus(TransferStatus.SUCCESS);
            } else {
                createNewTransfer.setStatus(TransferStatus.ERROR);
                log.debug("Money from Account{}", fromAccountId);
            }
            return transferMapper.makeTransfer(createNewTransfer);
        }
    }


}
