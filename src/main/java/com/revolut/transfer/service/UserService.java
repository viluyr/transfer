package com.revolut.transfer.service;

import com.revolut.transfer.dao.UserMapper;
import com.revolut.transfer.dto.UserEntity;
import com.revolut.transfer.exception.UnProcessableEntityException;
import com.revolut.transfer.init.MyBatisInitializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.javassist.NotFoundException;
import org.apache.ibatis.session.SqlSession;

import javax.inject.Inject;

@Slf4j
public class UserService {

    private final MyBatisInitializer myBatisInitializer;

    @Inject
    public UserService(MyBatisInitializer myBatisInitializer) {
        this.myBatisInitializer = myBatisInitializer;
    }

    public long createUser(UserEntity userEntity) throws UnProcessableEntityException {
        try (SqlSession sqlSession = myBatisInitializer.getSqlSessionFactory().openSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            if (userMapper.createUser(userEntity) <= 0) {
                throw new UnProcessableEntityException("Failed to create User");
            }
            log.debug("Can't create user{}", userEntity);
            return userEntity.getId();
        }
    }

    public UserEntity getUserByID(Long id) {
        try (SqlSession sqlSession = myBatisInitializer.getSqlSessionFactory().openSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            return userMapper.getUserByID(id);
        }
    }

    public UserEntity setAccountIDByUserId(Long id, Long accountId) {
        try (SqlSession sqlSession = myBatisInitializer.getSqlSessionFactory().openSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            log.debug("Set account id {}", id, accountId);
            return userMapper.setAccountIDByUserId(id, accountId);
        }
    }

    public Boolean delete(Long id) throws NotFoundException {
        try (SqlSession sqlSession = myBatisInitializer.getSqlSessionFactory().openSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            if (!userMapper.delete(id)) {
                throw new NotFoundException("Invalid User id"); // create my new excpt
            }
            log.debug("Can't delete user{}", id);
            return true;
        }
    }
}
