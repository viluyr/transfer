package com.revolut.transfer;


import com.revolut.transfer.exception.ServerStartException;
import com.revolut.transfer.init.MyBatisInitializer;
import com.revolut.transfer.init.ServerStart;

public class Application {
    public static void main(String[] args) throws  ServerStartException {
        MyBatisInitializer myBatisInitializer = new MyBatisInitializer();
        myBatisInitializer.getSqlSessionFactory();
        ServerStart serverStart = new ServerStart();
        serverStart.initServer();
    }

}