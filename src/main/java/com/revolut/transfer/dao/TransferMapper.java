package com.revolut.transfer.dao;

import com.revolut.transfer.dto.TransferEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TransferMapper {
    @Insert("INSERT INTO transfer (fromAccountID, toAccountID, amountOfTransferMoney, currency, status," +
            " dateOfTransactions) "
            + "VALUES (#{fromAccountID}, #{toAccountID}, #{amountOfTransferMoney}, #{currency}, #{status}, " +
            "#{dateOfTransactions})")
    @Options(useGeneratedKeys = true, keyColumn = "id")
    TransferEntity makeTransfer(final TransferEntity transferEntity);

    @Select("SELECT * FROM transfer")
    List<TransferEntity> getAll();

    @Select("SELECT * FROM transfer where id = #{fromAccountID} or id = #{toAccountID}")
    List<TransferEntity> getTransferByUserId(long id);
}
