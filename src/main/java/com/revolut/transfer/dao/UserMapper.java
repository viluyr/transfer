package com.revolut.transfer.dao;

import com.revolut.transfer.dto.UserEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserMapper {
    @Insert("INSERT INTO users  name, surname, accountID) "
            + "VALUES (#{name}, #{surname}, #{accountID})")
    @Options(useGeneratedKeys = true, keyColumn = "id")
    long createUser(final UserEntity userEntity);

    @Select("SELECT * FROM users WHERE id = #{id}")
    UserEntity getUserByID(@Param("id") final long id);

    @Update("UPDATE users SET accountID = #{accountID} WHERE id = #{id}")
    UserEntity setAccountIDByUserId(@Param("id") final long id, @Param("accountID") final long accountID);

    @Delete("DELETE FROM users WHERE id = #{id}")
    Boolean delete(final long id);

}
