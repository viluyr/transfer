package com.revolut.transfer.dao;

import com.revolut.transfer.dto.AccountEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface AccountMapper {
    @Insert("INSERT INTO account (accountID, amount, currency, status, transferID) "
            + "VALUES (#{id}, #{amount}, #{currency}, #{status},#{transferID} )")
    @Options(useGeneratedKeys = true, keyColumn = "accountID")
    long createAccount(final AccountEntity accountEntity);

    @Select("SELECT * FROM account WHERE accountID = #{accountID}")
    AccountEntity getAccountByID(@Param("accountID") final long id);

    @Update("UPDATE account SET amount = #{amount} WHERE accountID = #{accountID}")
    AccountEntity updateAccountBalanceById(@Param("accountID") final long accountID, double amount);

    @Update("UPDATE account SET transfer_id = #{transfer_id} WHERE accountID = #{accountID}")
    AccountEntity updateAccountAfterTransferById(@Param("accountID") final long accountID);

    @Delete("DELETE FROM account WHERE accountID = #{accountID}")
    Boolean delete(final Long id);

}
