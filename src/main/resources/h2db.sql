DROP ALL objects;

create table transfer
(
  id int auto_increment
  		primary key,
  fromAccountID int unsigned not null,
  toAccountID int unsigned not null,
  amountOfTransferMoney double unsigned not null,
  currency ENUM('DOLLAR', 'EURO')not null default 'DOLLAR',
status varchar(15)default 'active' not null,
dateOfTransactions date not null,
);

create table account
(
  accountID int unsigned not null,
  amount double unsigned not null,
  currency ENUM('DOLLAR', 'EURO')not null default 'DOLLAR',
status varchar(15)default 'active' not null,
transferID int,
primary key(accountID),
constraint  FK_ACCOUNT_TRANSFER
foreign key(transferID)references transfer (id)
);

create table users
(
	id int auto_increment primary key,
	name varchar(20) not null,
surname varchar(20)not null,
accountID int,
constraint FK_USERS_ACCOUNT
foreign key(accountID)references account (accountID)
);
