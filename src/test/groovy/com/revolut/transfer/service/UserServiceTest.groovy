package com.revolut.transfer.service

import com.revolut.transfer.dao.UserMapper
import com.revolut.transfer.dto.UserEntity
import com.revolut.transfer.init.MyBatisInitializer
import org.apache.ibatis.session.SqlSession
import org.apache.ibatis.session.SqlSessionFactory
import spock.lang.Specification
import spock.lang.Subject

class UserServiceTest extends Specification {

    UserEntity userEntity
    @Subject
    UserService sut
    MyBatisInitializer myBatisInitMock
    SqlSessionFactory sqlSessionFactoryMock
    UserMapper userMapperMock
    SqlSession sqlSessionMock

    void setup() {
        myBatisInitMock = Mock(MyBatisInitializer)
        sqlSessionFactoryMock = Mock(SqlSessionFactory)
        userMapperMock = Mock(UserMapper)
        sqlSessionMock = Mock(SqlSession)
        sut = new UserService(myBatisInitMock)

        userEntity = UserEntity.builder()
                .id(1)
                .name('Ivan')
                .surname('Ivanov')
                .accountID(1)
                .build()
    }

    def 'should create user'() {
        when:
        sut.createUser(userEntity)

        then:
        1 * myBatisInitMock.getSqlSessionFactory() >> sqlSessionFactoryMock
        1 * sqlSessionFactoryMock.openSession() >> sqlSessionMock
        1 * sqlSessionMock.getMapper(UserMapper) >> userMapperMock
        1 * userMapperMock.createUser(userEntity) >> userEntity.id
    }

    def 'should set account to user by id'() {
        when:
        sut.setAccountIDByUserId(1, 3)

        then:
        1 * myBatisInitMock.getSqlSessionFactory() >> sqlSessionFactoryMock
        1 * sqlSessionFactoryMock.openSession() >> sqlSessionMock
        1 * sqlSessionMock.getMapper(UserMapper) >> userMapperMock
        1 * userMapperMock.setAccountIDByUserId(_, _) >> userEntity
    }

    def 'should  delete user by id'() {
        when:
        sut.delete(1)

        then:
        1 * myBatisInitMock.getSqlSessionFactory() >> sqlSessionFactoryMock
        1 * sqlSessionFactoryMock.openSession() >> sqlSessionMock
        1 * sqlSessionMock.getMapper(UserMapper) >> userMapperMock
        1 * userMapperMock.delete(1) >> true
    }
}
