package com.revolut.transfer.service

import com.revolut.transfer.dao.AccountMapper
import com.revolut.transfer.dao.TransferMapper
import com.revolut.transfer.dto.AccountEntity
import com.revolut.transfer.dto.enums.TypeOfCurrency
import com.revolut.transfer.init.MyBatisInitializer
import org.apache.ibatis.session.SqlSession
import org.apache.ibatis.session.SqlSessionFactory
import spock.lang.Specification
import spock.lang.Subject

class AccountServiceTest extends Specification {

    AccountEntity fromAccountEntity
    AccountEntity toAccountEntity

    @Subject
    AccountService sut
    MyBatisInitializer myBatisInitMock
    SqlSessionFactory sqlSessionFactoryMock
    SqlSession sqlSessionMock
    AccountMapper accountMapperMock
    TransferMapper transferMapperMock


    void setup() {
        myBatisInitMock = Mock(MyBatisInitializer)
        sqlSessionFactoryMock = Mock(SqlSessionFactory)
        accountMapperMock = Mock(AccountMapper)
        sqlSessionMock = Mock(SqlSession)
        transferMapperMock = Mock(TransferMapper)
        sut = new AccountService(myBatisInitMock)

        fromAccountEntity = AccountEntity.builder()
                .accountID(1)
                .amount(1000)
                .currency(TypeOfCurrency.DOLLAR)
                .active('active')
                .build()

        toAccountEntity = AccountEntity.builder()
                .accountID(2)
                .amount(100)
                .currency(TypeOfCurrency.DOLLAR)
                .active('active')
                .build()

    }

    def 'should create account for user'() {
        when:
        sut.createAccount(fromAccountEntity)

        then:
        1 * myBatisInitMock.getSqlSessionFactory() >> sqlSessionFactoryMock
        1 * sqlSessionFactoryMock.openSession() >> sqlSessionMock
        1 * sqlSessionMock.getMapper(AccountMapper) >> accountMapperMock
        1 * accountMapperMock.createAccount(fromAccountEntity) >> fromAccountEntity.accountID
    }

    def 'should delete account by id'() {
        when:
        sut.delete(1)

        then:
        1 * myBatisInitMock.getSqlSessionFactory() >> sqlSessionFactoryMock
        1 * sqlSessionFactoryMock.openSession() >> sqlSessionMock
        1 * sqlSessionMock.getMapper(AccountMapper) >> accountMapperMock
        1 * accountMapperMock.delete(1) >> true

    }

    def 'should make money transfer between two account'() {
        given:
        long fromID = fromAccountEntity.getAccountID()
        long toID = toAccountEntity.getAccountID()
        double amount = 100.0

        when:
        sut.makeTransfer(fromID, toID, amount, TypeOfCurrency.DOLLAR)

        then:
        2 * myBatisInitMock.getSqlSessionFactory() >> sqlSessionFactoryMock
        2 * sqlSessionFactoryMock.openSession() >> sqlSessionMock
        1 * sqlSessionMock.getMapper(TransferMapper) >> transferMapperMock
        1 * sqlSessionMock.getMapper(AccountMapper) >> accountMapperMock
        1 * accountMapperMock.getAccountByID(fromID) >> fromAccountEntity
        1 * accountMapperMock.getAccountByID(toID) >> toAccountEntity

    }
}



