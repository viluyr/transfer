package com.revolut.transfer.service

import com.revolut.transfer.dto.UserEntity
import com.revolut.transfer.init.MyBatisInitializer
import com.revolut.transfer.init.ServerStart
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise


class UserServiceDevTest extends Specification {
    UserEntity userEntity
    @Shared
    ServerStart server
    @Shared
    UserService sut
    @Shared
    MyBatisInitializer myBatisInitializer

    def setup() {
        server = new ServerStart().initServer()
        myBatisInitializer = new MyBatisInitializer()
        userEntity = UserEntity.builder()
                .id(1)
                .name('Ivan')
                .surname('Ivanov')
                .accountID(1)
                .build()
    }

    def 'should create user'() {
        when:
       long id = sut.createUser(userEntity)
        then:
        assert id == 1
    }
}
