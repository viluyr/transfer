#---->TASK<----#

App for the transfer of money between accounts without the use spring and heavy frameworks

#---->Used technologies<----#


Application starts a jetty server on localhost port 8080,  H2 in memory database(database schema creation script)
Also, dependency injection is used.

- Embedded H2
- Mybatis (SQL Mapper)
- JAX-RS
- Logback
- Jetty Container
- Jersey
- Lombok
- Spock

#---->API<----#

There are three database tables - User Transfer and Account (To normalize the database)

#User#
[POST]
Create new user: {name, surname, accountID}
http://localhost:8080/user/create

[GET]
Get user by id: {id}
http://localhost:8080/user/

[PUT]
Update user: {id, accountID}
http://localhost:8080/user/update

[DELETE]
Delete user by id: {id}
http://localhost:8080/user/delete/

#Account#
[POST]
Create account for user: {accountID, amount, currency, status, transferID}
http://localhost:8080/bank/create/

[POST]
Create transfer between two accounts: {fromAccountID, toAccountID, amount, currency, status, date}
http://localhost:8080/bank/transfer

[DELETE]
Delete account: {accountID}
http://localhost:8080/bank/delete/

[GET]
Get account by id: {id}
http://localhost:8080/bank/
